import java.util.Scanner;

public class Equals {

	public static void main(String[] args) {
		System.out.println("Nhập vào chuỗi:");
		Scanner sc = new Scanner(System.in);
		String chuoi = sc.nextLine();
		char[] mangKyTu = chuoi.toCharArray();
		for(int i = 0; i < mangKyTu.length ; i++) {
			String kyTuHienTai = String.valueOf(mangKyTu[i]);
			if(kyTuHienTai.equals("a")) {
				System.out.println("Phát hiện ký tự a ở vị trí thứ : "+i);
			}
		}
	}

}
