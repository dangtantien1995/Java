import java.util.Scanner;

public class Main {

	int centuryFromYear(int year){
        int century;
        if (year < 100){
            century = 1;
            return century;
        }
        else{
            century = year / 100;
            if (year % 100 == 0){
                return century;
            }
            else{
                return century + 1;
            }
        }
    }
	
	
	public static void main(String[] args) {
		Main m = new Main();
		Scanner x= new Scanner(System.in);
		System.out.println("Nhap :");
		int num = x.nextInt();
		System.out.println("The ki : " + m.centuryFromYear(num));
	}

}
