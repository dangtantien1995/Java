import java.util.Scanner;

public class Sosanh {

	public static void main(String[] args) {
		System.out.println("Nhập vào chuỗi: ");
		Scanner sc = new Scanner(System.in);
		String chuoiVuaDuocNhapVao = sc.nextLine();
		boolean flag = false;
		char[] mangKiTu = chuoiVuaDuocNhapVao.toCharArray();
		for(int i = 0 ; i < mangKiTu.length ; i++) {
			String kiTuHienTai = String.valueOf(mangKiTu[i]);
			if ( kiTuHienTai.equals("u")) {
				System.out.println("Vị trí của kí tự u trong chuỗi là ở vị trí thứ : " + i);
				flag = true;
			}else {
				flag = false;
			}
		}
		
		if(flag == true) {
			System.out.println("Ki tu nay có tồn tại trong chuỗi");
		}
		
		if(flag == false) {
			System.out.println("Khong có ký tự này trong chuỗi");
		}
	}

}
